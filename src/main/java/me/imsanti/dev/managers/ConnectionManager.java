package me.imsanti.dev.managers;

import me.imsanti.dev.RedirectMessagesB;
import me.imsanti.dev.utils.ColorUtils;
import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.*;

public class ConnectionManager implements Listener {

    private final RedirectMessagesB redirectMessagesB;
    private final Map<UUID, Boolean> openServer = new HashMap<>();
    private final Map<UUID, String> connecting = new HashMap<>();

    public ConnectionManager(final RedirectMessagesB redirectMessagesB) {
        this.redirectMessagesB = redirectMessagesB;
    }

    @EventHandler
    public void handleConnection(final ServerConnectEvent event) {
        if(event.getTarget().getName().equals("Creative") && !openServer.containsKey(event.getPlayer().getUniqueId()) && !connecting.containsKey(event.getPlayer().getUniqueId())) return;
        if(openServer.getOrDefault(event.getPlayer().getUniqueId(), false).equals(false)) {
            event.setCancelled(true);
            openServer.put(event.getPlayer().getUniqueId(), false);
        }

        if(connecting.containsKey(event.getPlayer().getUniqueId()) && openServer.containsKey(event.getPlayer().getUniqueId()) && openServer.get(event.getPlayer().getUniqueId()).equals(true)) return;
        redirectMessagesB.getProxy().getServers().get(event.getTarget().getName()).ping(new Callback<ServerPing>() {

            @Override
            public void done(ServerPing result, Throwable error) {
                if(error==null) {
                   // if(event.getPlayer().getServer().getInfo().getName().equals(event.getTarget().getName())) return;

                    openServer.put(event.getPlayer().getUniqueId(), true);
                    if(!event.isCancelled()) {
                        event.setCancelled(true);
                    }else {
                        event.getPlayer().connect(event.getTarget());

                    }
                    connecting.put(event.getPlayer().getUniqueId(), event.getTarget().getName());
                    openServer.put(event.getPlayer().getUniqueId(), false);
                    return;
                }else {
                    event.setCancelled(true);
                    openServer.put(event.getPlayer().getUniqueId(), false);
                    event.getPlayer().sendMessage(ColorUtils.color(redirectMessagesB.getConfig().getString("server-closed")));
                }

            }
        });

        openServer.put(event.getPlayer().getUniqueId(), false);
        connecting.remove(event.getPlayer().getUniqueId());

    }

    @EventHandler
    public void handleLeave(final PlayerDisconnectEvent event) {
        openServer.remove(event.getPlayer().getUniqueId());
        connecting.remove(event.getPlayer().getUniqueId());
    }
}



