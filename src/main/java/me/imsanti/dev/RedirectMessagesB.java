package me.imsanti.dev;

import com.google.common.io.ByteStreams;
import me.imsanti.dev.managers.ConnectionManager;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.*;
import java.util.logging.Level;

public final class RedirectMessagesB extends Plugin {
    private Configuration config;

    @Override
    public void onEnable() {
        loadConfig();
        registerListeners();
        // Plugin startup logic
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    private void registerListeners() {
        getProxy().getPluginManager().registerListener(this, new ConnectionManager(this));
    }

    public void loadConfig() {
        try {
            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(loadResource("config.yml"));
        } catch (IOException e) {
            this.getLogger().log(Level.SEVERE, "Exception while reading config", e);
        }
    }

    public File loadResource(String resource) {
        File folder = this.getDataFolder();
        if (!folder.exists())
            folder.mkdir();
        File resourceFile = new File(folder, resource);
        try {
            if (!resourceFile.exists()) {
                resourceFile.createNewFile();
                try (InputStream in = this.getResourceAsStream(resource);
                     OutputStream out = new FileOutputStream(resourceFile)) {
                    ByteStreams.copy(in, out);
                }
            }
        } catch (Exception e) {
            this.getLogger().log(Level.SEVERE, "Exception while writing default config", e);
        }
        return resourceFile;
    }

    public Configuration getConfig() {
        return this.config;
    }
}
